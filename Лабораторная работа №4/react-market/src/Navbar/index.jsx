import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

export default class MyNavbar extends Component {

    render() {
        return (
            <Navbar inverse>
                <Navbar.Header>
                    <Navbar.Brand>
                        <a href="#home">Dikost'</a>
                    </Navbar.Brand>
                </Navbar.Header>
                <Nav>
                <NavItem href="#">SHOP</NavItem>
                <NavItem href="#">ABOUT US</NavItem>
                <NavItem href="#">ORDER</NavItem>
                <NavItem href="#">CONTACTS</NavItem>
                </Nav>
            </Navbar>
        )
    }
}
