import React, { Component } from 'react';
import { Button } from 'react-bootstrap';

import './style.css';

export default class Item extends Component {

    render() {
        return (
            <div className="item">
                <img src={this.props.data.src} alt={this.props.data.title} />
                <div className="title">{this.props.data.title}</div>
                <div className="price">{this.props.data.price}</div>
                <Button bsStyle="primary">Купить</Button>
            </div>
        )
    }
}