import React, { Component } from 'react';
import Item from '../Item';

import './style.css';

const bannedData = {
    title: 'Ключница "KeyHLDR"',
    price: '800р.',
    src: '/images/2.jpg'
}

export default class Banner extends Component {

    render() {
        return (
            <div className="banner">
                <div className="banner-title">Реклама</div>
                <Item data={bannedData}/>
            </div>
        )
    }
}