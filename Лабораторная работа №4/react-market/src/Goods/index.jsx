import React, { Component } from 'react';
import { Row, Col } from 'react-bootstrap';
import Item from '../Item';

import './style.css';

export default class Goods extends Component {

    render() {
        // let stationComponents = this.props.data.map(item => {
        //     return <div className="Product">{item.title}</div>;
        // });

        // return <div>{stationComponents}</div>;

        let stationComponents = this.props.data.map(item => {
            return (
                <Col xs={12} sm={6} md={4}>
                    <div className="goods-item">
                        <Item data={item}/>
                    </div>
                </Col>
            )
        });

        return (
            <Row>{stationComponents}</Row>
        )
    }
}