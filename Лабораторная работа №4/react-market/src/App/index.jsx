import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import './App.css';

import Goods from '../Goods';
import MyNavbar from '../Navbar';
import Banner from '../Banner';




class App extends Component {
    goods = [
        {title:'Портмоне "EXP"', price: '3200р.', src: "/images/1.jpg"},
        {title:'Портмоне "DOC"', price: '2000р.', src: "/images/3.jpg"},
        {title:'Бумажник "PURE"', price: '2300р.', src: "/images/4.jpg"},
        {title:'Блокнот"BLOCK"', price: '3000р.', src: "/images/5.jpg"},
        {title:'Блокнот"BLOCK"', price: '3000р.', src: "/images/5.jpg"},
        {title:'Блокнот"BLOCK"', price: '3000р.', src: "/images/5.jpg"}
    ];
  render() {
    return (
      <div className="App">
        <MyNavbar/>
          <Grid>
              <Row>
                <Col md={12} lg={9}>
                  <Goods data={this.goods}/>
                </Col>
                <Col xsHidden smHidden mdHidden lg={3}>
                  <Banner />
                </Col>
              </Row>
          </Grid>
      </div>
    );
  }
}

export default App;
