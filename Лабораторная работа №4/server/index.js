const express = require('express');
const app = express();

const PORT = 2999;

app.use('/images', express.static(__dirname + '/public'));

app.listen(PORT, () => {
    console.log('Server started on port ' + PORT);
});
