
const app = require('express')();

const hostname = '127.0.0.1';
const port = 3000;
let data = [];

app.get('/', function(req, res) {
    res.send('Hello World');
});
app.get('/api', function(req, res) {
    res.send('API is running');
});
app.get('/api/products', function(req, res) {
    res.json(data.map(item => {
        return {
            id: item.id,
            name: item.name,
            price: item.price
        };
    }));
});
app.get('/api/products/:id', function(req, res) {
    res.json(data[parseInt(req.params.id)-1]);
});

app.listen(port, hostname, function(){
    console.log('Server started on port ' + port);

    const fs = require('fs');
    data = JSON.parse(fs.readFileSync('data.json'));
});
