export interface Ivehicle {
    ride();
    stop();

    x_pos: number;
    y_pos: number;
    is_stopped: boolean;
    fuel: number;
    name: string;
}