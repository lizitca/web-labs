import { Ivehicle } from "./ivehicle";

export class Wrecker implements Ivehicle {
    constructor(target: Ivehicle) {
        this.target = target;
        this.x_pos = 0;
        this.y_pos = 0;
        this.is_stopped = false;
        this.name = 'Wrecker';
        this.fuel = 1000;
        this.is_target_reached = false;
    }

    ride() {
        if (!this.is_target_reached && !this.is_stopped) {
            this.x_pos += 10;
            if (this.target.x_pos == this.x_pos) {
                this.is_target_reached = true;
            }
        }
    }
    stop() {
        throw new Error("Method not implemented.");
    }
    x_pos: 0;
    y_pos: 0;
    is_stopped: boolean;
    fuel: number;
    name: string;

    target: Ivehicle;
    is_target_reached: boolean;
}