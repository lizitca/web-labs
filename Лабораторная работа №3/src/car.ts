import {Ivehicle} from "./ivehicle";

export class Car implements Ivehicle {
    name: string;
    x_pos: number;
    y_pos: number;
    is_stopped: boolean;
    fuel: number;

    constructor() {
        this.x_pos = 0;
        this.y_pos = 0;
        this.is_stopped = false;
    }

    ride() {
        if (!this.is_stopped) {
            this.fuel--;
            this.x_pos += this.speed;
            if (this.fuel <= 0) {
                this.is_stopped = true;
            }
        }
    }

    stop() {
    }

    color: string;
    speed: number;
    weight: number;
}
