import { Car } from './car'

export class Toyota extends Car {
    constructor() {
        super();
        this.name = "Toyota";
        this.fuel = 55;
    }

    color: string;
    speed: number;
    weight: number;

    ride() {
        super.ride();
    }

    stop() {
    }

}