import { Ivehicle } from "./ivehicle";
import { Toyota } from "./toyota";
import { Kawasaki } from "./kawasaki";
import { Wrecker } from "./wrecker";
import { Draw } from "./draw";

export class Road {
    // vehicles: Ivehicle[] = [];
    vehicles: {[key: string]: Ivehicle} = Object.create(null);
    wreckers: {[key: string]: Wrecker} = Object.create(null);

    constructor(){
        let toyota = new Toyota();
        toyota.color = '#ff0000';
        toyota.speed = 90;
        toyota.weight = 1000;
        this.add(toyota);

        let kawasaki = new Kawasaki();
        kawasaki.color = '#cccc';
        kawasaki.speed = 25;
        kawasaki.weight = 225;
        kawasaki.type = 'cruser';
        this.add(kawasaki);
    }

    start(){
        setInterval(() => {
            console.log('---------------------------');
            for (let key in this.vehicles) {
                let vehicle = this.vehicles[key];
                vehicle.ride();
                Draw.DrawVehicle(vehicle);

                if (vehicle.is_stopped) {
                    this.addWrecker(vehicle);
                }
            }
            for (let key in this.wreckers) {
                let wrecker = this.wreckers[key];
                wrecker.ride();
                Draw.DrawWrecker(wrecker);

                if (wrecker.is_target_reached) {
                    this.evacuate(wrecker);
                }
            }
        }, 250);
    }

    add(t: Ivehicle){
        if (!this.vehicles[t.name]) {
            this.vehicles[t.name] = t;
        }
    }

    addWrecker(v: Ivehicle) {
        if (!this.wreckers[v.name]) {
            this.wreckers[v.name] = new Wrecker(v);
        }
    }

    evacuate(w: Wrecker) {
        delete this.wreckers[w.target.name];
        delete this.vehicles[w.target.name];
    }
}
