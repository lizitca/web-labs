import { Motorcycle } from "./motorcycle"

export class Kawasaki extends Motorcycle {
    constructor() {
        super();
        this.name = 'Kawasaki';
        this.fuel = 35;
    }

    ride() {
        super.ride();
    }
}
