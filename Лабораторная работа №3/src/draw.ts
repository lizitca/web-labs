import { Ivehicle } from "./ivehicle";
import { Wrecker } from "./wrecker";

export class Draw {
    static DrawVehicle(vehicle: Ivehicle) {
        console.log(`${vehicle.name}. Fuel: ${vehicle.fuel} X: ${vehicle.x_pos} Y: ${vehicle.y_pos} IsStopped: ${vehicle.is_stopped}`);
    }

    static DrawWrecker(wrecker: Wrecker) {
        console.log(`${wrecker.name}. Target: ${wrecker.target.name} X: ${wrecker.x_pos} Y: ${wrecker.y_pos}`);
    }
}
